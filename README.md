### My setup

I run coreboot with Grub as a payload on my x230. This setup isn't the best, but it works. I change it when I get bored.

Grub will find the first partition of the first disk it finds, mount it and load another grub config file called `loader.cfg`.

It contains hand-written boot entries and customizations.

Compatibility with most distributions can be achieved by loading `/grub/grub.cfg` instead. 

### Steps to replicate:

Script to update the BIOS region with the newly built coreboot is provided.
You will need to boot linux with `iomem=relaxed` for internal flashing to work. 

I fully recommend having a ch341a_spi flasher and a SOIC-8 clip just in case the current coreboot code doesn't build into something bootable. 

1. Clone coreboot source

    `git clone https://review.coreboot.org/coreboot.git`

2. Put defconfig as .config inside your coreboot source directory

    `cp defconfig ./coreboot/.config`
    
3. Review the configuration

    `cd coreboot`
    
    `make nconfig`
  
4. Make toolchain and image

    `make crossgcc CPUS=4`
    
    `make`

5. Flash image

    `cd ..`

    `sudo ./flash.sh`
